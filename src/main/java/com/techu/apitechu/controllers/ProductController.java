package com.techu.apitechu.controllers;


import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {

    static final String APIBaseUrl = "/apitechu/v1";


    @GetMapping(APIBaseUrl + "/products")
    public ArrayList<ProductModel> getProducts(){
        System.out.println("getProducts");
        return ApitechuApplication.productModels;
    }

    @GetMapping(APIBaseUrl+"/products/{id}")
    public ProductModel getProductById(@PathVariable String id){
        System.out.println("getProductById");
        System.out.println("id es "+id);

        ProductModel result = new ProductModel();
        for (ProductModel product : ApitechuApplication.productModels){
            if (product.getId().equals(id)){
                result = product;
                break;
            }
        }
        return result;
    }

    @PostMapping(APIBaseUrl+"/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct){
        System.out.println("createProduct");
        System.out.println("La ID del nuevo producto es "+newProduct.getId());
        System.out.println("La descripcion del nuevo producto es "+newProduct.getDesc());
        System.out.println("El precio del nuevo producto es "+newProduct.getPrice());
        ApitechuApplication.productModels.add(newProduct);
        return newProduct;
    }

    @PutMapping(APIBaseUrl+"/products/{id}")
    public ProductModel updateProduct(@PathVariable String id, @RequestBody ProductModel product){
        System.out.println("updateProduct");
        System.out.println("La id recibida por parametro es: " + id);
        System.out.println("La id del producto a atualizar es: " + product.getId());
        System.out.println("La descripción del producto a atualizar es: " + product.getDesc());
        System.out.println("El precio del producto a atualizar es: " + product.getPrice());
        ProductModel result = new ProductModel();
        for (ProductModel productInList : ApitechuApplication.productModels){
            if (productInList.getId().equals(id)){
                productInList.setId(product.getId());
                productInList.setDesc(product.getDesc());
                productInList.setPrice(product.getPrice());
                return productInList;
            }
        }
        System.out.println("El id no corresponde con ningún producto registrado");
        return product;
    }

    @DeleteMapping(APIBaseUrl+"/products/{id}")
    public ProductModel deleteProduct (@PathVariable String id){
        ProductModel result = new ProductModel();
        System.out.println("deleteProduct");
        System.out.println("La id recibida por parametro es: " + id);
        for (ProductModel productInList : ApitechuApplication.productModels){
            if (productInList.getId().equals(id)){
                result = productInList;
                break;
            }
        }
        ApitechuApplication.productModels.remove(result);
        return result;
    }
    @PatchMapping(APIBaseUrl+"/products/{id}")
    public ProductModel patchProduct (@PathVariable String id, @RequestBody ProductModel product){
        System.out.println("patchProduct");
        System.out.println("La id recibida por parametro es: " + id);
        System.out.println("La descripción del producto a atualizar es: " + product.getDesc());
        System.out.println("El precio del producto a atualizar es: " + product.getPrice());
        for (ProductModel productInList : ApitechuApplication.productModels){
            if (productInList.getId().equals(id)){
                if (product.getDesc()!=null) {
                    System.out.println("Actualizando descripción");
                    productInList.setDesc(product.getDesc());
                }
                if (product.getPrice() > 0) {
                    System.out.println("Actualizando precio");
                    productInList.setPrice(product.getPrice());
                }
                return(productInList);
            }
        }
        System.out.println("El id no corresponde con ningún producto registrado");
        return new ProductModel();
    }

}
